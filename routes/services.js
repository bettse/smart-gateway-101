function serviceIndex(request, reply) {
  var node_uuid = request.params.node_id;

  var peripheral = request.server.app.peripherals.find(function(peripheral) {
    return (peripheral.address === node_uuid);
  });
  var services = request.server.app.services[peripheral.address].map(function(service) {
    return serviceModel(peripheral, service, request);
  });
  reply({services: services});
}

function serviceShow(request, reply) {
  var node_uuid = request.params.node_id;
  var service_uuid = request.params.service_id;

  var peripheral = request.server.app.peripherals.find(function(peripheral) {
    return (peripheral.address === node_uuid);
  });
  var service = request.server.app.services[peripheral.address].find(function(service) {
    return (service.uuid == service_uuid);
  });
  reply(serviceModel(peripheral, service, request));
}

function serviceModel(peripheral, service, request) {
    return {
      self    : { href : request.to('service#show', {params: {node_id: peripheral.address, service_id: service.uuid}})},
      handle  : service.handle,
      uuid    : service.uuid,
      primary : true
    }
}

module.exports = [
  { method: 'GET', path:'/gatt/nodes/{node_id}/services', handler: serviceIndex, config: {id: 'service#index' }},
  { method: 'GET', path:'/gatt/nodes/{node_id}/services/{service_id}', handler: serviceShow, config: {id: 'service#show' }}
]
