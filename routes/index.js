var nodes = require('./nodes');
var services = require('./services');
var characteristics = require('./characteristics');
var values = require('./values');
module.exports = [].concat(nodes, services, characteristics, values);
