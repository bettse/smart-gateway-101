function nodeIndex(request, reply) {
  var reply_json = request.server.app.peripherals.map(function(peripheral) {
    var services = request.server.app.services[peripheral.address];
    return nodeModel(peripheral, services, request);
  });
  reply({"nodes" : reply_json});
}

function nodeShow(request, reply) {
  var node_uuid = request.params.node_id;
  var peripheral = request.server.app.peripherals.find(function(peripheral) {
    return (peripheral.address === node_uuid);
  });
  var services = request.server.app.services[peripheral.address];
  reply(nodeModel(peripheral, services, request));
}

function nodeModel(peripheral, services, request) {
  return {
    self: { href: request.to('node#show', { params: { node_id: peripheral.address } }) },
    handle: peripheral.id,
    advertisement: peripheral.advertisement,
    bdaddrs: [{
      bdaddr: peripheral.address,
      bdaddrType: peripheral.addressType
    }],
    service: services,
    rssi: peripheral.rssi,
    AD: [{
      ADType: "<type1>",
      ADValue: "<value1>"
    }]
  };
}

module.exports = [
  {
    method: 'GET',
    path: '/gap/nodes',
    handler: nodeIndex,
    config: {id: 'node#index'}
  },
  {
    method: 'GET',
    path: '/gap/nodes/{node_id}',
    handler: nodeShow,
    config: {id: 'node#show'}
  }
]
