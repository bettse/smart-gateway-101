/* global Buffer */
var BeanMessage = require('../lib/bean_message');
var messageBuilder = new BeanMessage();
var async = require('async');
var BeanData = require('../lib/bean_data');

function sendValue(peripheral, request, reply) {
  var service_uuid = request.params.service_id;
  var characteristic_uuid = request.params.characteristic_id;
  var value = new Buffer(request.params.value, "hex");

  peripheral.discoverSomeServicesAndCharacteristics(
    [service_uuid],
    [characteristic_uuid],
    function(error, services, characteristics) {
      if(error){
        reply(new Error(error));
        console.error(error);
        peripheral.disconnect();
      } else {
        var characteristic = characteristics[0]
        characteristic.write(value, false, function(error) {
          if(error) {
            reply(new Error(error));
            console.error(error);
            peripheral.disconnect();
          }
        });
        setTimeout(function(){
          peripheral.disconnect()
        },300);
        reply("OK");
      }
    }
  );
}

module.exports = [
  {
    method: 'GET',
    path:'/gatt/nodes/{node_id}/services/{service_id}/characteristics/{characteristic_id}/value',
    handler: function (request, reply) {
      var node_uuid = request.params.node_id;
      var service_uuid = request.params.service_id;
      var characteristic_uuid = request.params.characteristic_id;
      var async = require('async');
      var peripheral;
      var characteristics_json = [];
      var host = request.server.info.host + ':' + request.server.info.host;

      if(request.server.app.cached_value) {
        reply(request.server.app.cached_value)
      } else {
        request.server.app.peripherals.forEach(function(node){
          if(node.address === node_uuid) {
            peripheral = node
          }
        })
        peripheral.connect(function(error) {
          if(error) {
            reply(new Error(error))
          } else {
            peripheral.discoverSomeServicesAndCharacteristics(
              [service_uuid],
              [characteristic_uuid],
              function(error, services, characteristics) {
                if(error){
                  reply(new Error(error));
                  peripheral.disconnect();
                } else {
                  var characteristic = characteristics[0]
                  characteristic.read(function(error, data) {
                    if(error) {
                      reply(new Error(error));
                      peripheral.disconnect();
                    }
                    else {
                      reply(
                        {
                          "self" : {
                            "href" : "http://" + host + "/gatt/nodes/" + peripheral.address + "/characteristics/" + characteristic.uuid
                          },
                          "handle" : characteristic.name,
                          "properties" : characteristic.properties,
                          "value"  : data
                        }
                      )
                      peripheral.disconnect();
                    }
                  })
                }
              }
            );
          }
        })
      }
    }
  },
  {
    method: 'PUT',
    path:'/gatt/nodes/{node_id}/services/{service_id}/characteristics/{characteristic_id}/value/{value}',
    handler: function (request, reply) {
      var node_uuid = request.params.node_id;
      var service_uuid = request.params.service_id;
      var characteristic_uuid = request.params.characteristic_id;
      var host = request.server.app.host;

      var peripheral;

      request.server.app.peripherals.forEach(function(node){
        if(node.address === node_uuid) {
          peripheral = node
        }
      })

      if(peripheral && peripheral.state != 'connected') {
        peripheral.connect(function(error) {
          if(error) {
            console.error("PROBLEM WRITING", error);
          }
          sendValue(peripheral, request, reply);
        });
      } else {
        sendValue(peripheral, request, reply);
      }
    }
  }
];
