"use strict";

var noble = require('noble');
var Hapi = require('hapi');
var routes = require('./routes');
var async =require('async');
var server = new Hapi.Server();
var debug = require('debug')('gateway:discovery')

server.connection({
  port: 8001
});
var host = server.info.host + ':' + server.info.port;

server.route(routes);

server.app.peripherals = [];
server.app.services = [];
server.app.characteristics = [];
server.app.characteristic_values = [];
server.app._characteristics = {};
server.app.descriptors = [];

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    noble.startScanning()
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function(peripheral) {
  debug("found a node: ", peripheral.address);

  server.app.peripherals.push(peripheral);
  server.app.characteristics[peripheral.address] = {};
  server.app.characteristic_values[peripheral.address] = {};
  server.app._characteristics[peripheral.address] = {};
  server.app.services[peripheral.address] = [];
  server.app.descriptors[peripheral.address] = {};
})

noble.on('scanStart', function() {
  debug('on -> scanStart');
  setTimeout(function() {
    noble.stopScanning();
  }, 10 * 1000);
})

noble.on('scanStop', function() {
  debug('on -> scanStop');
  mapPeripherals();
});

function mapPeripherals() {
  server.app.peripherals.map(function(peripheral){
    debug("Peripheral", peripheral.uuid);
    peripheral.connect(function(error) {
      if (error){
        console.error(error);
      }
      async.waterfall([async.apply(discoverServices, peripheral), discoverChars], final)
    });
  })
}

function discoverServices(peripheral, callback){
  // Get services
  peripheral.discoverServices(null, function(error, services){
    if (error){
      console.error(error);
    }
    services.forEach(function(service){
      debug("\t", "service", service.uuid);
      server.app.services[peripheral.address].push({
        "uuid": service.uuid,
        "handle": service.name
      })
      server.app.characteristics[peripheral.address][service.uuid] = [];
      server.app.characteristic_values[peripheral.address][service.uuid] = {};
      server.app._characteristics[peripheral.address][service.uuid] = {};
      server.app.descriptors[peripheral.address][service.uuid] = {};
    });
    callback(null, peripheral, services);
  })
}

function discoverChars(peripheral, services, callback){
  services.forEach(function(service){
    service.discoverCharacteristics(null, function(error, characteristics) {
      if (error){
        console.error(error);
      }
      characteristics.forEach(function(characteristic) {
        debug("\t\t", "characteristic", characteristic.uuid);

        server.app.descriptors[peripheral.address][service.uuid][characteristic.uuid] = [];
        server.app._characteristics[peripheral.address][characteristic._serviceUuid][characteristic.uuid] = characteristic;

        server.app.characteristics[peripheral.address][characteristic._serviceUuid].push({
          "self" : {
            "href" : "http://" + host + "/gatt/nodes/" + peripheral.address + "/characteristics/" + characteristic.uuid
          },
          "handle"     : characteristic.name,
          "uuid"       : characteristic.uuid,
          "properties" : characteristic.properties
        });

        characteristic.discoverDescriptors(function(error, descriptors){
          descriptors.forEach(function(descriptor){
            server.app.descriptors[peripheral.address][service.uuid][characteristic.uuid] = descriptor;
          })
        });
      });
      callback(null, peripheral);
    });
  })
}


function final(error, peripheral) {
  if (error){
    console.error(error);
  }

  //close connection
  peripheral.disconnect(function(error){
    if (error){
      console.error(error);
    }
    debug('Disconnect called');
  });
}


/** @description Start server.
*/
server.register(require('hapi-to'), function (err) {
    if (err) {
        throw err;
    }

    server.start();
});
